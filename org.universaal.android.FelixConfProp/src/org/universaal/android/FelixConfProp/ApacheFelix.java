package org.universaal.android.FelixConfProp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;

import org.apache.felix.framework.FrameworkFactory;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.launch.Framework;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.universaal.android.FelixConfProp.view.ViewFactory;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ApacheFelix extends Activity {
	
	private File m_cache;
	private static final String CONF_FILE="/sdcard/data/felix-confprop/conf/config.properties";
	private ServiceTracker m_tracker;
	private Framework m_fwk;
	private Properties m_configProps;
	private Properties m_sysProps;
	private TextView mRateText; 
	
    /** Called when the activity is first created. 
     * @throws IOException */
    @Override
    public synchronized void onCreate(Bundle aContext){
    	super.onCreate(aContext);
    	prepareUI();
        prepareFelix();
    }


    
    @Override
    public synchronized void onStart() {
    	super.onStart();
	    startFelix();
		// Register for OSGI services implementing Android UIs
	    try {
			m_tracker = new ServiceTracker(m_fwk.getBundleContext(), 
					m_fwk.getBundleContext().createFilter("(" + Constants.OBJECTCLASS + "=" + ViewFactory.class.getName() + ")"), 
						new ServiceTrackerCustomizer() {

						@Override
						public Object addingService(ServiceReference ref) {
						    System.out.println("======= Service found !");
							final ViewFactory fac = (ViewFactory) m_fwk.getBundleContext().getService(ref);
							if (fac != null) {
								runOnUiThread(new Runnable() {
									public void run() {
										setContentView(fac.create(ApacheFelix.this));
									}
								});
							}	
							return fac;
						}

						@Override
						public void modifiedService(ServiceReference ref,
								Object service) {
							// TODO Auto-generated method stub
							removedService(ref, service);
							addingService(ref);
						}

						@Override
						public void removedService(ServiceReference ref,
								Object service) {
							m_fwk.getBundleContext().ungetService(ref);
							// TODO Auto-generated method stub
							runOnUiThread(new Runnable() {
								public void run() {
									prepareUI();
								}
							});
						}});
			m_tracker.open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void prepareFelix(){
    	//Read configuration properties.
	    m_configProps=new Properties();
	    m_sysProps=new Properties();
	    InputStream in;
		try {
			//Load the Felix and OSGi properties
			in = new FileInputStream(CONF_FILE);
			m_configProps.load(in);
			in.close();
			//Copy all other properties to system properties
			m_sysProps=System.getProperties();
			m_sysProps.putAll(m_configProps);
			System.setProperties(m_sysProps);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if (m_configProps == null)
	    {
	        System.err.println("No properties file found.");
	        m_configProps = new Properties();
	    }
	    
	    // Use the specified bundle cache directory over default.
	    try {
			m_cache = File.createTempFile("/felix-cache", null, getCacheDir());
		} catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
        m_cache.delete();
        try{
        	if (!m_cache.mkdirs()) {
        		throw new IllegalStateException("Unable to create bundles dir");
        	}
        	if (m_cache != null)
    	    {
    	        m_configProps.setProperty(Constants.FRAMEWORK_STORAGE, m_cache.getAbsolutePath());
    	    }
    	}catch (Exception e) {
    		Log.v("out", e.getMessage());
		}
    }
    
    private void startFelix(){
    	try
	    {
	        // Create an instance and initialize the framework.
	        FrameworkFactory factory = FrameworkFactory.class.newInstance();
	        m_fwk = factory.newFramework(m_configProps);
	        m_fwk.init();
	        // Start the framework.
	        m_fwk.start();
	        // Install & start bundles from the conf file
	        AutoProcessor.process(m_configProps, m_fwk.getBundleContext());
	    }
	    catch (Exception ex)
	    {
	        System.err.println("Could not create framework: " + ex);
	        ex.printStackTrace();
	        System.exit(0);
	    }
    }
    
    protected void prepareUI(){
    	setContentView(R.layout.main);
        mRateText = (TextView) findViewById(R.id.outText);
        PrintStream out = new PrintStream(new OutputStream(){
        	ByteArrayOutputStream output = new ByteArrayOutputStream();
			@Override
			public void write(int oneByte) throws IOException {
				output.write(oneByte);
				if (oneByte == '\n') {
					Log.v("out", new String(output.toByteArray()));
					runOnUiThread(new Runnable() {
						public void run() {
							mRateText.setText(mRateText.getText()+output.toString()+"\n");
						}
					});
					output = new ByteArrayOutputStream();
				}
			}});
        Button psButton=(Button) findViewById(R.id.Button01);
		psButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_fwk!=null){
					org.osgi.framework.Bundle[] bun=m_fwk.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						Log.v("out", bun[i].getSymbolicName()+"==="+getstate(bun[i].getState()));
					}
				}
			}
		});
		Button servButton=(Button) findViewById(R.id.Button02);
		servButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_fwk!=null){
					org.osgi.framework.Bundle[] bun=m_fwk.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						ServiceReference[] ref=bun[i].getRegisteredServices();
						if(ref!=null){
							Log.v("out", bun[i].getSymbolicName());
							for(int j=0;j<ref.length;j++){
								Log.v("out", "   |_"+ref[j].toString());
							}
						}
					}
				}
			}
		});
		Button startButton=(Button) findViewById(R.id.Button03);
		startButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_fwk!=null){
					org.osgi.framework.Bundle[] bun=m_fwk.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						try {
							bun[i].start();
						} catch (BundleException e) {
							Log.v("out",e.getMessage());
						}
					}
				}
			}
		});
        System.setErr(out);
        System.setOut(out);
    }
    
    @Override
    public synchronized void onStop() {
    	super.onStop();
    	System.out.println("============= ON STOP ==========");
    	if(m_tracker!=null){
    		m_tracker.close();
    		m_tracker = null;
    	}
    	try {
    		m_fwk.stop();
		} catch (BundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m_fwk = null;
    }
    
    @Override
    public synchronized void onDestroy() {
    	super.onDestroy();
        System.out.println("============= ON DESTROY ==========");
    	delete(m_cache);
    	m_cache = null;
    }
    
    private void delete(File target) {
    	if (target.isDirectory()) {
    		for (File file : target.listFiles()) {
    			delete(file);
    		}
    	}
    	target.delete();
    }
    
    private String getstate(int id){
    	switch (id) {
		case org.osgi.framework.Bundle.ACTIVE:
			return "ACTIVE";
		case org.osgi.framework.Bundle.INSTALLED:
			return "INSTALLED";
		case org.osgi.framework.Bundle.RESOLVED:
			return "RESOLVED";
		case org.osgi.framework.Bundle.STARTING:
			return "STARTING";
		case org.osgi.framework.Bundle.STOPPING:
			return "STOPPING";
		case org.osgi.framework.Bundle.UNINSTALLED:
			return "UNINSTALLED";
		default:
			return "UNKNOWN";
		}
    }
}