/*
	Copyright 2011 ITACA-TSB, http://www.tsb.upv.es
	Instituto Tecnologico de Aplicaciones de Comunicacion 
	Avanzadas - Grupo Tecnologias para la Salud y el 
	Bienestar (TSB)
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universAAL.android.karaf;

import java.io.File;
import java.util.Properties;

import org.apache.karaf.main.Main;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.universAAL.android.felix.IActivityStub;
import org.universAAL.android.felix.IContextStub;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class KarafService extends Service{

	private static final int ONGOING_NOTIFICATION = 13879;
	private static final String LOG_NAME="KarafService";
	private File m_cache;
	private Main m_fwk=null;
	private MulticastLock m_lock;
	private IActivityStub m_iohandler=null;
	private Activity m_activityHandle=null;
	private ServiceTracker m_tracker;
	private boolean m_karafrunning=false;
	private OSGiBinder m_Binder=new OSGiBinder();
	private IContextStub m_context=new ContextStubImpl();
	
	@Override
	public void onCreate() {
		Log.d(LOG_NAME, "Creating Service");
		// Load the Felix and OSGi properties
		try { // TODO monitor mounted media, and auto generate blank file if not present
			System.setProperty("karaf.home", "/mnt/sdcard/data/karaf");
			System.setProperty("javax.xml.validation.SchemaFactory:http://www.w3.org/2001/XMLSchema", "org.webpki.android.org.apache.xerces.jaxp.validation.XMLSchemaFactory");
			// Use android cache dir for bundle cache instead of felix default.
			m_cache = File.createTempFile("/felix-cache", null, getCacheDir());
			m_cache.delete();
			if (!m_cache.mkdirs()) {
				throw new IllegalStateException("Unable to create bundles dir");
			}
			if (m_cache != null) {
				System.setProperty(Constants.FRAMEWORK_STORAGE,m_cache.getAbsolutePath());
			}
			// Create instance of framework
			m_fwk = new Main(new String[]{});
		} catch (Exception e) {
			Toast.makeText(this, "Error starting Service. Karaf is not running", Toast.LENGTH_LONG).show();
			Log.e(LOG_NAME, e.toString());
			e.printStackTrace();
			return;
		}
		// Create notification to be used to access the GUI
		Notification notif = new Notification(R.drawable.ic_stat_notify,
				getText(R.string.notif_text), System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, KarafActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notif.setLatestEventInfo(this, getText(R.string.notif_title),
				getText(R.string.notif_message), pendingIntent);
		// Make foreground to run forever... ? here?
		startForeground(ONGOING_NOTIFICATION, notif);
		// Setup wifi multicast to connect other nodes
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		m_lock = wifi.createMulticastLock("karaf_multicast");
		m_lock.acquire();
		// Start Karaf in a thread because Activity holds until onCreate returns
		// Would make sense in onStart, but that is called in every screen reorientation
		Thread thr = new Thread(new KarafRunner());
		thr.setPriority(Thread.MAX_PRIORITY);//TODO: safe? battery? Remove if remote process?
		thr.start();
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(LOG_NAME,"Binding Service");
		return m_Binder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(LOG_NAME,"Unbinding Service");
		m_activityHandle=null;
		updateUIlink();
		return false;
	}

	@Override
	public void onDestroy() {
		Log.d(LOG_NAME,"Stopping Service");
		// TODO: UNRegister for OSGI services implementing Android UIs?
		try {
			m_fwk.destroy();
			m_karafrunning=false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Free resources and quit service from foreground: it can be auto killed
		m_fwk = null;
		m_lock.release();
		stopForeground(true);
		delete(m_cache); //TODO: Really destroys cache?
		m_cache = null;
	}
	
	/**
	 * Links/unlinks GUI of Activity to IO Handler, if any.
	 */
	private void updateUIlink(){
		Log.d(LOG_NAME,"Updating UI link");
		if (m_iohandler != null && m_activityHandle!=null) {
			Log.d(LOG_NAME,"Updating UI link: We have HANDLER and ACTIVITY");
			m_activityHandle.runOnUiThread(new Runnable() {//TODO: no run on UI?
				public void run() {
					m_activityHandle.setContentView(m_iohandler.buildViewFromActivity(m_activityHandle));
				}
			});
		}else if(m_iohandler == null && m_activityHandle!=null){
			Log.d(LOG_NAME,"Updating UI link: We don�t have HANDLER but have ACTIVITY");
			m_activityHandle.runOnUiThread(new Runnable() {//TODO: no run on UI?
				public void run() {
					m_activityHandle.setContentView(R.layout.splash);
					TextView loadtext=(TextView)m_activityHandle.findViewById(R.id.textView1);
					// If m_karafrunning is true it means the handler left or not come yet.
					loadtext.setText(m_karafrunning?R.string.splash_waitui:R.string.splash_loading);
					Animation anim=AnimationUtils.loadAnimation(m_activityHandle, android.R.anim.fade_in);
			        anim.setRepeatCount(Animation.INFINITE);
			        anim.setDuration(3000);
			        loadtext.startAnimation(anim);
				}
			});
		}else if(m_iohandler != null && m_activityHandle==null){
			Log.d(LOG_NAME,"Updating UI link: We have HANDLER but don�t have ACTIVITY");
			m_iohandler.buildViewFromActivity(m_activityHandle);
		}
	}
	
	/**
	 * Recursive method to wipe out cache
	 * 
	 * @param target
	 *            Folder to remove
	 */
	private void delete(File target) {
		if (target.isDirectory()) {
			for (File file : target.listFiles()) {
				delete(file);
			}
		}
		target.delete();
	}
	
	//---------------Classes--------------------
	
	//TODO: Use IPC handlers so that service runs in different process : faster?
	public class OSGiBinder extends Binder {
		void linkActivity(final Activity act) {//TODO: FINAL??????
			// Got the activity. Let�s see if we have a IO handler to link it to.
			m_activityHandle=act;
			updateUIlink();
		}
	}
	
	// Official implementation of IContextStub
	public class ContextStubImpl implements IContextStub{
		@Override
		public Context getAndroidContext() {
			//Return this service�s context
			return KarafService.this; //TODO .getbaseContext??? will affect handler?
		}
	}
	
	protected class KarafRunner implements Runnable{
		@Override
		public void run() {
			Log.d("KarafRunner","Trying to run Karaf");
			try {
				m_fwk.launch();
				// Register for OSGI services implementing Android UIs
				m_tracker = new ServiceTracker(m_fwk.getFramework().getBundleContext(), 
						m_fwk.getFramework().getBundleContext().createFilter(
								"(" + Constants.OBJECTCLASS + "="
										+ IActivityStub.class.getName() + ")"),
						new ActivityStubTracker());
				m_tracker.open();
				//publish in OSGi the android context, for the bundles that want to use it without UI
				m_fwk.getFramework().getBundleContext().registerService(IContextStub.class.getName(), m_context, null);
            } catch (Throwable ex) {
                // Also log to sytem.err in case logging is not yet initialized
                System.err.println("Could not launch framework: " + ex);
                ex.printStackTrace();
                try {
					m_fwk.destroy();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                m_fwk.setExitCode(-1);
            }
			
			//When karaf is done starting bundles and all is up, change splash
			if(m_activityHandle!=null)m_activityHandle.runOnUiThread(new Runnable() {
				public void run() {
					m_activityHandle.setContentView(R.layout.splash);
			        TextView loadtext=(TextView)m_activityHandle.findViewById(R.id.textView1);
			        //m_activityHandle may be in hands of a IOhandler, and textView1 not present
			        if(loadtext!=null)loadtext.setText(R.string.splash_waitui);
			        Animation anim=AnimationUtils.loadAnimation(m_activityHandle, android.R.anim.fade_in);
			        anim.setRepeatCount(Animation.INFINITE);
			        anim.setDuration(3000);
			        loadtext.startAnimation(anim);
				}
			});
			// Only used for UI small tricks, not important
			m_karafrunning=true;

            try {
            	m_fwk.awaitShutdown();
                boolean stopped = m_fwk.destroy();
                if (!stopped) {
                        System.err.println("Timeout waiting for framework to stop.  Exiting VM.");
                        m_fwk.setExitCode(-3);
                }
            } catch (Throwable ex) {
            	m_fwk.setExitCode(-2);
                System.err.println("Error occurred shutting down framework: " + ex);
                ex.printStackTrace();
            } finally {
                    System.exit(m_fwk.getExitCode());
            }
		}
	}
	
	// Updates UI link when new handlers arrive (or leave)
	protected class ActivityStubTracker implements ServiceTrackerCustomizer{
		@Override
		public Object addingService(ServiceReference ref) {
			Log.d("ActivityStubTracker","Found a UI implementor");
			m_iohandler = (IActivityStub) m_fwk.getFramework().getBundleContext().getService(ref);
			updateUIlink();
			return m_iohandler;//TODO: manage a pile of IActivityStub
		}
		@Override
		public void modifiedService(ServiceReference ref, Object service) {
			Log.d("ActivityStubTracker","Found changed UI implementor");
			removedService(ref, service);
			addingService(ref);
			updateUIlink();
		}
		@Override
		public void removedService(ServiceReference ref, Object service) {
			Log.d("ActivityStubTracker","Removed a UI implementor");
			m_fwk.getFramework().getBundleContext().ungetService(ref);
			m_iohandler=null;//TODO: manage a pile of IActivityStub
			updateUIlink();
		}
	}
}
