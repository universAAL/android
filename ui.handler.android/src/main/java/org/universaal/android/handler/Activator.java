package org.universaal.android.handler;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.android.felix.IActivityStub;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.uAALBundleContainer;
import org.universAAL.middleware.container.utils.LogUtils;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.ui.UIHandlerProfile;
import org.universAAL.middleware.ui.UIRequest;
import org.universAAL.middleware.ui.owl.Modality;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;

public class Activator implements BundleActivator{
	
	private LinearLayout m_MainView;
	private BundleContext m_context;
	private Object m_Factory;
	private AndroidHandler m_handler;
	protected static ModuleContext m_moduleContext;

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		m_context = context;
		m_moduleContext = uAALBundleContainer.THE_CONTAINER
				.registerModule(new Object[] { context });
		m_Factory=new InnerHandlerViewFactory();
		m_handler=new AndroidHandler(m_moduleContext,getOutputSubscriptionParams());
        m_context.registerService(IActivityStub.class.getName(), m_Factory, null);
        LogUtils.logDebug(m_moduleContext, Activator.class, "start",
    		    new Object[] { "Started the Android IO handler" }, null);
	}

	/* (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		m_handler.close();
		m_handler=null;
	}
	
	/** I am interested in all events with following restrictions
	 * @return The profile with my interest
	 */
	private UIHandlerProfile getOutputSubscriptionParams() {
		UIHandlerProfile oep = new UIHandlerProfile();
		oep.addRestriction(MergedRestriction.getFixedValueRestriction(
			UIRequest.PROP_PRESENTATION_MODALITY, Modality.gui));
		return oep;
	}

	/**
	 * Class that implements the android activity stub. The Felix-Android app
	 * monitors for these implementations and when it finds one, it gives it
	 * the control over the app GUI.
	 * 
	 * @author alfiva
	 * 
	 */
	public class InnerHandlerViewFactory implements IActivityStub{

		/* (non-Javadoc)
		 * @see org.universAAL.android.felix.IActivityStub#buildViewFromActivity(android.app.Activity)
		 */
		public View buildViewFromActivity(Activity activity) {
			if (activity!=null){
				activity.setTheme(android.R.style.Theme_Light);
				activity.setTitle("universAAL");
				activity.setTitleColor(Color.WHITE);
				m_MainView = new LinearLayout(activity);
				m_MainView.setOrientation(LinearLayout.VERTICAL);
			}else{
				m_MainView=null;
			}
			m_handler.setViewAndActivity(m_MainView, activity);
			LogUtils.logDebug(m_moduleContext, Activator.class, "buildViewFromActivity",
	    		    new Object[] { "Created the Android handler" }, null);
			return m_MainView;
		}

	}
	

}
