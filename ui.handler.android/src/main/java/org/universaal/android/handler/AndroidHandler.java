package org.universaal.android.handler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.util.BundleConfigHome;
import org.universAAL.middleware.container.utils.LogUtils;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.middleware.ui.UIHandler;
import org.universAAL.middleware.ui.UIHandlerProfile;
import org.universAAL.middleware.ui.UIRequest;
import org.universAAL.middleware.ui.UIResponse;
import org.universAAL.middleware.ui.rdf.Form;
import org.universAAL.middleware.ui.rdf.FormControl;
import org.universAAL.middleware.ui.rdf.Group;
import org.universAAL.middleware.ui.rdf.Input;
import org.universAAL.middleware.ui.rdf.InputField;
import org.universAAL.middleware.ui.rdf.Label;
import org.universAAL.middleware.ui.rdf.MediaObject;
import org.universAAL.middleware.ui.rdf.Range;
import org.universAAL.middleware.ui.rdf.Repeat;
import org.universAAL.middleware.ui.rdf.Select;
import org.universAAL.middleware.ui.rdf.Select1;
import org.universAAL.middleware.ui.rdf.SimpleOutput;
import org.universAAL.middleware.ui.rdf.Submit;
import org.universAAL.middleware.ui.rdf.TextArea;
import org.universAAL.middleware.util.Constants;
import org.universAAL.ontology.profile.User;

import android.R;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AndroidHandler extends UIHandler{

//	private DefaultInputPublisher m_ip;
	private LinearLayout m_MainViewHandle;
	private Activity m_ActivityHandle;
	private View controlsView;
	private View submitsView;
	private boolean m_mute=false;
	private UIRequest currentOutput=null;
	static final String singleUserURI=Constants.uAAL_MIDDLEWARE_LOCAL_ID_PREFIX+"saied";
	private static File confHome = new File(new BundleConfigHome("android.handler").getAbsolutePath());
	
	/** Default constructor
	 * @param context uAAL Module Context
	 * @param initialSubscription Initial handler profile for subscription
	 */
	protected AndroidHandler(ModuleContext context,
			UIHandlerProfile initialSubscription) {
		super(context, initialSubscription);
//		m_ip = new DefaultInputPublisher(m_bundlecontext);
		//Initial input-> main menu
//		InputEvent event=new InputEvent(new User(singleUserURI),null,InputEvent.uAAL_MAIN_MENU_REQUEST);
//		m_ip.publish(event);
		this.userLoggedIn(new User(singleUserURI),null);
	}
	
	/** Links the handler to the android activity and view
	 * @param mainView Android view
	 * @param activity Android activity
	 */
	protected void setViewAndActivity(LinearLayout mainView, Activity activity){
		m_MainViewHandle=mainView;
		m_ActivityHandle=activity;
		if(currentOutput!=null){
			handleUICall(currentOutput);
		}
	}

	/* (non-Javadoc)
	 * @see org.universAAL.middleware.ui.UIHandler#adaptationParametersChanged(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public void adaptationParametersChanged(String arg0, String arg1,
			Object arg2) {
		// TODO For now do nothing
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"adaptationParametersChanged",
				new Object[] { "adaptation params changed" }, null);
	}

	/* (non-Javadoc)
	 * @see org.universAAL.middleware.ui.UIHandler#communicationChannelBroken()
	 */
	@Override
	public void communicationChannelBroken() {
		// TODO For now do nothing
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"communicationChannelBroken",
				new Object[] { "comm channel broken" }, null);
	}

	/* (non-Javadoc)
	 * @see org.universAAL.middleware.ui.UIHandler#cutDialog(java.lang.String)
	 */
	@Override
	public Resource cutDialog(String dialogID) {
		// TODO For now do nothing
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"cutDialog", new Object[] { "cut dialog" }, null);
		return null;
	}

	/* (non-Javadoc)
	 * @see org.universAAL.middleware.ui.UIHandler#handleUICall(org.universAAL.middleware.ui.UIRequest)
	 */
	@Override
	public void handleUICall(final UIRequest event) {
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"handleUICall", new Object[] { "handle o event" }, null);
		currentOutput=event;
		if(m_MainViewHandle!=null || m_ActivityHandle!=null){
		//Handle user? not for now
		//This is where operations commented below should be
		m_ActivityHandle.runOnUiThread(new Runnable() {
			public void run() {
				/* These next operations should be outside the runOnUiThread, 
				 * to avoid performing lengthy operations in UI thread, 
				 * but don�t know why they wouldn�t work there for the Galaxy Tab.
				 * They do work there in "official" Android (and on my HTC).
				 */
				Form f=event.getDialogForm();
				if(f.isSystemMenu()){
					controlsView=renderGroupControl(event.getDialogForm().getIOControls(),true);
					submitsView=renderGroupControl(event.getDialogForm().getStandardButtons(),false);
				}else if(f.isMessage()){
					controlsView=renderGroupControl(event.getDialogForm().getIOControls(),true);
					submitsView=renderGroupControl(event.getDialogForm().getSubmits(),false);
				}else{
					controlsView=renderGroupControl(event.getDialogForm().getIOControls(),true);
					submitsView=renderGroupControl(event.getDialogForm().getSubmits(),false);
				}
				//...until here
				
				ScrollView m_Controls = new ScrollView(m_ActivityHandle);
				HorizontalScrollView m_Submits = new HorizontalScrollView(m_ActivityHandle);
		       
				if(controlsView!=null)
				m_Controls.addView(controlsView);
				if(submitsView!=null)
				m_Submits.addView(submitsView);
				
				ImageView separator=new ImageView(m_ActivityHandle);
				Drawable sepDraw=Drawable.createFromPath(confHome+"/separator.png");
				if(sepDraw!=null)
					separator.setBackgroundDrawable(sepDraw);
				
				m_MainViewHandle.removeAllViews();
				m_MainViewHandle.setBackgroundColor(Color.rgb(240,240,240));

				m_MainViewHandle.addView(m_Controls,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,1));
				m_MainViewHandle.addView(separator,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,0));
				m_MainViewHandle.addView(m_Submits,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,0));
		        
				m_MainViewHandle.setVisibility(View.VISIBLE);
				m_MainViewHandle.postInvalidate();
			}
		});
			LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
					"handleUICall", new Object[] { "invalidated" }, null);
		}
	}
	
	/** When pressed some button in the screen, send the response
	 * @param submit The submit that was pressed, along with all it s info
	 */
	public void performSubmit(Submit submit) {
//		Object o = submit.getFormObject().getProperty(UIRequest.MY_URI);
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"performSubmit", new Object[] { "pressed submit" }, null);
		this.dialogFinished(new UIResponse(new User(singleUserURI), null, submit));
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"performSubmit", new Object[] { "submit processed" }, null);
		// this.dialogFinished(submit, false);//TODO: dialog type
		// m_ip.publish(new InputEvent(new User(singleUserURI), null, submit));
	}
	
	//============================END HANDLER=============================

	//============================RENDERERS===============================
	// Every renderer that introduces a button (submit) will have in its
	// "onclick" method the activation of the process for "inputpublish". In
	// this process all "store inputs" will be made

	private View renderGroupControl(Group ctrl, boolean vertical, LinearLayout current) {
		LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
				"renderGroupControl", new Object[] { "rendering group" }, null);
		// Default error view for not adding/returning null
		TextView errorOut = new TextView(m_ActivityHandle);
		LinearLayout currentView;
		if(current==null){	
			currentView = new LinearLayout(m_ActivityHandle);
			currentView.setOrientation(vertical ? LinearLayout.VERTICAL
					: LinearLayout.HORIZONTAL);
		}else{
			currentView = current;
		}
		try {
			FormControl[] children = ctrl.getChildren();
			if (children == null || children.length == 0){
				errorOut.setText("Empty View!!!");
				errorOut.setTextColor(Color.GRAY);
				return null;
			}

			for (int i = 0; i < children.length; i++) {
				if (children[i] instanceof InputField) {
					renderInputControl(currentView,(InputField) children[i]);
				} else if (children[i] instanceof SimpleOutput) {
					renderOutputControl(currentView,(SimpleOutput) children[i]);
				} else if (children[i] instanceof Select1) {
					renderSelect1Control(currentView,(Select1) children[i]);
				} else if (children[i] instanceof Select) {
					renderSelectControl(currentView,(Select) children[i]);
				} else if (children[i] instanceof Repeat) {
					renderRepeat(currentView,(Repeat) children[i]);
				} else if (children[i] instanceof Group) {
					if(children[i].getLabel()!=null)
						if(children[i].getLabel().getText()!=null){
							TextView label=new TextView(m_ActivityHandle,null,R.attr.listSeparatorTextViewStyle);
							label.setText(children[i].getLabel().getText());
							currentView.addView(label);
						}
					View group=renderGroupControl((Group) children[i],vertical);
					if(group!=null)currentView.addView(group);
				} else if (children[i] instanceof Submit) {
					// also instances of SubdialogTrigger can be treated the
					// same
					renderSubmitControl(currentView,(Submit) children[i],vertical);
				} else if (children[i] instanceof MediaObject) {
					renderMediaObject(currentView,(MediaObject) children[i]);
				} else if (children[i] instanceof TextArea) {
					renderTextArea(currentView,(TextArea) children[i]);
				} else if (children[i] instanceof Range) {
					renderSpinnerControl(currentView,(Range) children[i]);
				} else {
					currentView.addView(errorOut); // TODO return error view
				}
				//separator
				if(vertical&&(i+1 < children.length)){
					ImageView separator=new ImageView(m_ActivityHandle);
					Drawable sepDraw=Drawable.createFromPath(confHome+"/separatorlist.png");
					if(sepDraw!=null)
						separator.setBackgroundDrawable(sepDraw);
					currentView.addView(separator,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,0));
				}
			}
		} catch (Exception e) {
			LogUtils.logDebug(Activator.m_moduleContext, AndroidHandler.class,
					"renderGroupControl", new Object[] { "problems rendering group" }, e);
			errorOut.setText("Null View!!!");
			errorOut.setTextColor(Color.RED);
			return errorOut;
		}
		//group separator
		if(vertical){
			ImageView separator=new ImageView(m_ActivityHandle);
			Drawable sepDraw=Drawable.createFromPath(confHome+"/separator.png");
			if(sepDraw!=null)
				separator.setBackgroundDrawable(sepDraw);
			currentView.addView(separator,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,0));
		}
		return currentView;
	}

	private View renderGroupControl(Group ctrl, boolean vertical) {
		return renderGroupControl(ctrl, vertical,null);
	}

	private void renderSpinnerControl(LinearLayout currentView, Range range) {
		renderLabel(currentView, range);
		Spinner spin=new Spinner(m_ActivityHandle);
		ArrayAdapter adapter = new ArrayAdapter(m_ActivityHandle, android.R.layout.simple_spinner_item);
		int max=(Integer)range.getMaxValue();
		int min=(Integer)range.getMinValue();
		int delta=Integer.parseInt(range.getStep().toString());
		Integer initVal=(Integer)range.getValue();
		int val=(initVal!=null)?initVal:min;
		int initial=val;
		for (int i = min; i < max; i+=delta) {
			adapter.add(new Integer(i));
			if(val==i)initial=i-min;
		}
		spin.setAdapter(adapter);
		spin.setOnItemSelectedListener(new SpinListener(range));
		spin.setSelection(initial);
		currentView.addView(spin);
		renderHintAndHelp(currentView, range);
	}

	private void renderTextArea(LinearLayout currentView, TextArea textArea) {
		renderLabel(currentView, textArea);
		EditText text = new EditText(m_ActivityHandle);
		text.setLines(3);
		text.setTextSize(20.0f);
		if(textArea.getValue()!=null){
			text.setText(textArea.getValue().toString());
		}
		text.addTextChangedListener(new InputListener(textArea));
		currentView.addView(text);
		renderHintAndHelp(currentView, textArea);
	}

	private void renderMediaObject(LinearLayout currentView, MediaObject mediaObject) {
		renderLabel(currentView, mediaObject);
		ImageView img=new ImageView(m_ActivityHandle);
		Drawable draw=Drawable.createFromPath(confHome+"/"+mediaObject.getContentURL());
		if(draw!=null)
			img.setImageDrawable(draw);
		else
			img.setImageDrawable(Drawable.createFromPath(confHome+"/notfound.png"));
		img.setAdjustViewBounds(true);
		img.setMaxHeight(70);
		currentView.addView(img);
		renderHintAndHelp(currentView,mediaObject);
	}

	private void renderSubmitControl(LinearLayout currentView, Submit submit,boolean vertical) {
		Button button=new Button(m_ActivityHandle);
		button.setText(submit.getLabel().getText());
		button.setTextColor(Color.rgb(9,96,130));
		button.setTextSize(22.0f);
		button.setOnClickListener(new SubmitListener(submit));
		button.setPadding(20, 0, 20, 0);
		if(!vertical)button.setMaxWidth(155);
		Drawable buttondraw=Drawable.createFromPath(confHome+"/button.png");
		if(buttondraw!=null)
			button.setBackgroundDrawable(buttondraw);
		if(button.getHint()!=null)
			button.setHint(button.getHint());
		currentView.addView(button);
	}

	private void renderRepeat(LinearLayout currentView, Repeat repeat) {
		//Title style label
		if(repeat.getLabel()!=null)
			if(repeat.getLabel().getText()!=null){
				TextView label=new TextView(m_ActivityHandle,null,R.attr.listSeparatorTextViewStyle);
				label.setText(repeat.getLabel().getText());
				currentView.addView(label);
			}
		
		FormControl[] elems = repeat.getChildren();
		boolean groupflag=false;
		TableLayout table=new TableLayout(m_ActivityHandle);
		if (elems == null  ||  elems.length != 1)
			throw new IllegalArgumentException("Malformed argument!");
		if (elems[0] instanceof Group) {
			groupflag=true;
			FormControl[] elems2 = ((Group) elems[0]).getChildren();
			if (elems2 == null  ||  elems2.length == 0)
				throw new IllegalArgumentException("Malformed argument!");
			TableRow row=new TableRow(m_ActivityHandle);
			for(int i=0;i<elems2.length;i++){
				if(elems2[i].getLabel()!=null){
					renderLabel(row, elems2[i]);
				}else{
					TextView label=new TextView(m_ActivityHandle);
					label.setText(elems2[i].getType());
					label.setTextColor(Color.rgb(9,96,130));
					label.setTextSize(22.0f);
					row.addView(label);
				}
			}
			table.addView(row);
		} else if (elems[0] == null)
			throw new IllegalArgumentException("Malformed argument!");
		for(int i=0;i<repeat.getNumberOfValues();i++){
			TableRow row=new TableRow(m_ActivityHandle);
			repeat.setSelection(i);
			m_mute=true;
			renderGroupControl((groupflag?(Group)elems[0]:repeat),false,row);
			m_mute=false;
			table.addView(row);
		}
		table.setShrinkAllColumns(true);
		table.setStretchAllColumns(false);
		currentView.addView(table);
		renderHintAndHelp(currentView, repeat);
	}

	private void renderSelectControl(LinearLayout currentView, Select select) {
		renderLabel(currentView, select);
		Label[] labels=select.getChoices();
		if(labels!=null){
			
			//TODO: Initial value?????????????
			LinearLayout checkgroup=new LinearLayout(m_ActivityHandle);
			checkgroup.setOrientation(LinearLayout.VERTICAL);
			CheckBox[] opts=new CheckBox[labels.length];
			ArrayList list=new ArrayList(labels.length); 
			for(int i=0;i<labels.length;i++){
				opts[i]=new CheckBox(m_ActivityHandle);
				opts[i].setText(labels[i].getText());
				opts[i].setTextSize(20.0f);
				opts[i].setOnCheckedChangeListener(new CheckListener(select,list,i));
				checkgroup.addView(opts[i]);
			}
			currentView.addView(checkgroup);
		}
		renderHintAndHelp(currentView,select);
	}

	private void renderSelect1Control(LinearLayout currentView, Select1 select1) {
		renderLabel(currentView, select1);
		Label[] labels=select1.getChoices();
		if(labels!=null){
			//TODO: Initial value?????????????????????????
			RadioGroup radiogroup=new RadioGroup(m_ActivityHandle);
			RadioButton[] opts=new RadioButton[labels.length];
//			Label initVal=(Label)select1.getValue();
			for(int i=0;i<labels.length;i++){
				opts[i]=new RadioButton(m_ActivityHandle);
				opts[i].setText(labels[i].getText());
				opts[i].setTextSize(20.0f);
				radiogroup.addView(opts[i]);
//				if(initVal!=null&&initVal.getText().equals(labels[i].getText()))
//					radiogroup.check(opts[i].getId());
			}
			currentView.addView(radiogroup);
			radiogroup.setOnCheckedChangeListener(new RadioListener(select1));
		}
		renderHintAndHelp(currentView,select1);
	}

	private void renderOutputControl(LinearLayout currentView, SimpleOutput simpleOutput) {
		renderLabel(currentView, simpleOutput);
		if(simpleOutput.getValue()!=null){
			TextView text = new TextView(m_ActivityHandle);
			text.setText(simpleOutput.getValue().toString());
			text.setTextSize(20.0f);
			currentView.addView(text);
		}
		renderHintAndHelp(currentView,simpleOutput);
	}

	private void renderInputControl(LinearLayout currentView, InputField inputField) {
		if(inputField.isOfBooleanType()){
			CheckBox mark=new CheckBox(m_ActivityHandle);
			if(inputField.getLabel()!=null)
				if(inputField.getLabel().getText()!=null){
					mark.setText(inputField.getLabel().getText());
				}
			mark.setTextSize(20.0f);
			if((Boolean)inputField.getValue()!=null){
				mark.setChecked((Boolean)inputField.getValue());
			}
			mark.setOnCheckedChangeListener(new InputListener(inputField));
			currentView.addView(mark);
		}else{
			renderLabel(currentView, inputField);
			EditText text = new EditText(m_ActivityHandle);
			text.setSingleLine();
			text.setTextSize(20.0f);
			if(inputField.getValue()!=null){
				text.setText(inputField.getValue().toString());
			}
			text.addTextChangedListener(new InputListener(inputField));
			currentView.addView(text);
		}
		renderHintAndHelp(currentView, inputField);
	}
	
	private void renderHintAndHelp(LinearLayout currentView,FormControl ctrl) {
		if(ctrl.getHelpString()!=null){
			TextView help=new TextView(m_ActivityHandle);
			help.setText(ctrl.getHelpString());
			currentView.addView(help);
			currentView.setContentDescription(ctrl.getHelpString());
		}
		if(ctrl.getHintString()!=null){
			TextView hint=new TextView(m_ActivityHandle);
			hint.setText(ctrl.getHintString());
			currentView.addView(hint);
			currentView.setContentDescription(ctrl.getHintString());
		}
	}

	private void renderLabel(LinearLayout currentView, FormControl ctrl){
		if(!m_mute){
			if(ctrl.getLabel()!=null)
				if(ctrl.getLabel().getText()!=null){
					TextView label=new TextView(m_ActivityHandle);
					label.setText(ctrl.getLabel().getText());
					label.setTextColor(Color.rgb(9,96,130));
					label.setTextSize(22.0f);
					currentView.addView(label);
				}
		}
	}
	
	//==============================END RENDERERS===============================
	
	//==============================LISTENERS===================================
	
	public class SubmitListener implements OnClickListener {
		private Submit submit;
		SubmitListener(Submit sub){
			submit=sub;
		}
		public void onClick(View arg0) {
			performSubmit(submit);
		}
	}

	public class SpinListener implements OnItemSelectedListener {
		private Range range;
		SpinListener(Range rng){
			range=rng;
		}
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			range.storeUserInput((Integer)range.getMinValue()+arg2);
		}
		public void onNothingSelected(AdapterView<?> arg0) {
			range.storeUserInput(null);
		}

	}
	
	public class RadioListener implements android.widget.RadioGroup.OnCheckedChangeListener {
		private Select1 select1;
		RadioListener(Select1 sel){
			select1=sel;
		}
		public void onCheckedChanged(RadioGroup arg0, int arg1) {
			RadioButton button=(RadioButton)m_ActivityHandle.findViewById(arg1);
			String txt=button.getText().toString();
			select1.storeUserInputByLabelString(txt);
		}
		
	}
	
	public class CheckListener implements OnCheckedChangeListener {
		private Select select;
		private List list;
		private int index;
		CheckListener(Select sel,List lst,int ind){
			select=sel;
			list=lst;
			index=ind;
		}
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			if(arg1){
				list.add(select.getChoices()[index]);
			}else{
				list.remove(select.getChoices()[index]);
			}
			select.storeUserInput(list);
		}
		
	}
	
	public class InputListener implements TextWatcher, OnCheckedChangeListener {
		private Input input;
		InputListener(Input inp){
			input=inp;
		}
		public void afterTextChanged(Editable text) {
			if(input.isOfBooleanType()){
				input.storeUserInput(text.toString());
			}else{
				input.storeUserInput(text.toString());
			}
		}
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// Nothing
		}
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// Nothing
		}
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			input.storeUserInput((Boolean.valueOf(arg1)));
		}
	}

	//==============================END LISTENERS===============================

}
