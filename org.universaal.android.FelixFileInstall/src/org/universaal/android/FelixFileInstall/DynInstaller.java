package org.universaal.android.FelixFileInstall;

import java.io.InputStream;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import android.content.res.Resources;

public class DynInstaller implements BundleActivator {
    
    private String PATH="";
    private Resources res;
    private int rawId;
    public DynInstaller(Resources res, int id, String path) {
        this.res = res;
        this.rawId = id;
        PATH=path;
    }
    
    @Override
    public void start(BundleContext arg0) throws Exception {
    	//When activated, load the jar of the bundle and install & start
        InputStream is = res.openRawResource(rawId);
        Bundle bundle = arg0.installBundle(PATH, is);
        bundle.start();
    }

    @Override
    public void stop(BundleContext arg0) throws Exception {
        // TODO Auto-generated method stub
    	// DO something here?
    }

}
