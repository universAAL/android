package org.universaal.android.FelixFileInstall;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.felix.framework.Felix;
import org.apache.felix.framework.Logger;
import org.apache.felix.framework.cache.BundleCache;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.framework.util.StringMap;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.universaal.android.FelixFileInstall.view.ViewFactory;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ApacheFelix extends Activity {
	public static final String FELIX_BUNDLES_DIR = "/sdcard/data/felix-fileinstall/bundles";
	
	private static final String ANDROID_FRAMEWORK_PACKAGES = (
			 "org.osgi.framework; version=1.5.0, "+
			 "org.osgi.framework.launch; version=1.0.0, "+
			 "org.osgi.framework.hooks.service; version=1.0.0, "+
			 "org.osgi.service.packageadmin; version=1.2.0, "+
			 "org.osgi.service.startlevel; version=1.1.0, "+
			 "org.osgi.service.url; version=1.0.0, "+
			 "org.osgi.util.tracker; version=1.4.0, "+
	            "android; " + 
	            "android.app;" + 
	            "android.content;" + 
	            "android.database;" + 
	            "android.database.sqlite;" + 
	            "android.graphics; " + 
	            "android.graphics.drawable; " + 
	            "android.graphics.glutils; " + 
	            "android.hardware; " + 
	            "android.location; " + 
	            "android.media; " + 
	            "android.net; " + 
	            "android.opengl; " + 
	            "android.os; " + 
	            "android.provider; " + 
	            "android.sax; " + 
	            "android.speech.recognition; " + 
	            "android.telephony; " + 
	            "android.telephony.gsm; " + 
	            "android.text; " + 
	            "android.text.method; " + 
	            "android.text.style; " + 
	            "android.text.util; " + 
	            "android.util; " + 
	            "android.view; " + 
	            "android.view.animation; " + 
	            "android.webkit; " + 
	            "android.widget; " + 
	            "com.google.android.maps; " + 
	            "com.google.android.xmppService; " + 
	            "javax.crypto; " + 
	            "javax.crypto.interfaces; " + 
	            "javax.crypto.spec; " + 
	            "javax.microedition.khronos.opengles; " + 
	            "javax.net; " + 
	            "javax.net.ssl; " + 
	            "javax.security.auth; " + 
	            "javax.security.auth.callback; " + 
	            "javax.security.auth.login; " + 
	            "javax.security.auth.x500; " + 
	            "javax.security.cert; " + 
	            "javax.sound.midi; " + 
	            "javax.sound.midi.spi; " + 
	            "javax.sound.sampled; " + 
	            "javax.sound.sampled.spi; " + 
	            "javax.sql; " + 
	            "javax.xml.parsers; " + 
	            "junit.extensions; " + 
	            "junit.framework; " + 
	            "org.apache.commons.codec; " + 
	            "org.apache.commons.codec.binary; " + 
	            "org.apache.commons.codec.language; " + 
	            "org.apache.commons.codec.net; " + 
	            "org.apache.commons.httpclient; " + 
	            "org.apache.commons.httpclient.auth; " + 
	            "org.apache.commons.httpclient.cookie; " + 
	            "org.apache.commons.httpclient.methods; " + 
	            "org.apache.commons.httpclient.methods.multipart; " + 
	            "org.apache.commons.httpclient.params; " + 
	            "org.apache.commons.httpclient.protocol; " + 
	            "org.apache.commons.httpclient.util; " + 
	            "org.bluez; " + 
	            "org.json; " + 
	            "org.w3c.dom; " + 
	            "org.xml.sax; " + 
	            "org.xml.sax.ext; " + 
	            "org.xml.sax.helpers; " + 
	            "version=1.0.0.m5-r15," +
	            "org.universaal.android.FelixFileInstall.view").intern();
	
	private File m_cache;
	private Felix m_felix;
	private StringMap m_configMap;
	private ServiceTracker m_tracker;
	private TextView mRateText; 
	
    /** Called when the activity is first created. 
     * @throws IOException */
    @Override
    public synchronized void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.main);
        mRateText = (TextView) findViewById(R.id.outText);
        PrintStream out = new PrintStream(new OutputStream(){
        	ByteArrayOutputStream output = new ByteArrayOutputStream();
			@Override
			public void write(int oneByte) throws IOException {
				output.write(oneByte);
				if (oneByte == '\n') {
					Log.v("out", new String(output.toByteArray()));
					runOnUiThread(new Runnable() {
						public void run() {
							mRateText.setText(mRateText.getText()+output.toString()+"\n");
						}
					});
					output = new ByteArrayOutputStream();
				}
			}});
        Button psButton=(Button) findViewById(R.id.Button01);
		psButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_felix!=null){
					org.osgi.framework.Bundle[] bun=m_felix.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						Log.v("out", bun[i].getSymbolicName()+"==="+getstate(bun[i].getState()));
					}
				}
			}
		});
		Button servButton=(Button) findViewById(R.id.Button02);
		servButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_felix!=null){
					org.osgi.framework.Bundle[] bun=m_felix.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						ServiceReference[] ref=bun[i].getRegisteredServices();
						if(ref!=null){
							Log.v("out", bun[i].getSymbolicName());
							for(int j=0;j<ref.length;j++){
								Log.v("out", "   |_"+ref[j].toString());
							}
						}
					}
				}
			}
		});
		Button startButton=(Button) findViewById(R.id.Button03);
		startButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(m_felix!=null){
					org.osgi.framework.Bundle[] bun=m_felix.getBundleContext().getBundles();
					for(int i=0;i<bun.length;i++){
						try {
							bun[i].start();
						} catch (BundleException e) {
							Log.v("out",e.getMessage());
						}
					}
				}
			}
		});
        System.setErr(out);
        System.setOut(out);
        prepareFelix();
    }
    
    @Override
    public synchronized void onStart() {
    	super.onStart();
		startFelix();
		// Register for OSGI services implementing Android UIs (NOT TESTED, UNCOMMENT AT OWN RISK)
		/*try {
			m_tracker = new ServiceTracker(m_felix.getBundleContext(), 
					m_felix.getBundleContext().createFilter("(" + Constants.OBJECTCLASS + "=" + ViewFactory.class.getName() + ")"), 
						new ServiceTrackerCustomizer() {

						@Override
						public Object addingService(ServiceReference ref) {
						    System.out.println("======= Service found !");
							final ViewFactory fac = (ViewFactory) m_felix.getBundleContext().getService(ref);
							if (fac != null) {
								runOnUiThread(new Runnable() {
									public void run() {
										setContentView(fac.create(ApacheFelix.this));
									}
								});
							}	
							return fac;
						}

						@Override
						public void modifiedService(ServiceReference ref,
								Object service) {
							// TODO Auto-generated method stub
							removedService(ref, service);
							addingService(ref);
						}

						@Override
						public void removedService(ServiceReference ref,
								Object service) {
							m_felix.getBundleContext().ungetService(ref);
							// TODO Auto-generated method stub
							runOnUiThread(new Runnable() {
								public void run() {
									setContentView(new View(ApacheFelix.this));
								}
							});
						}});
			m_tracker.open();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }
    
    private void prepareFelix(){
    	// Set the bundles directory where all bundles will be picked from
        File bundles = new File(FELIX_BUNDLES_DIR);
        if (!bundles.exists()) {
        	try{
	        	if (!bundles.mkdirs()) {
	        		throw new IllegalStateException("Unable to create bundles dir");
	        	}
        	}catch (Exception e) {
        		Log.v("out", e.getMessage());
			}
        }
        // Explicitly specify the directory to use for caching bundles.
		try {
			m_cache = File.createTempFile("/felix-cache", null, getCacheDir());
		} catch (IOException ex) {
			throw new IllegalStateException(ex);
		}
        m_cache.delete();
        try{
        	if (!m_cache.mkdirs()) {
        		throw new IllegalStateException("Unable to create bundles dir");
        	}
    	}catch (Exception e) {
    		Log.v("out", e.getMessage());
		}
    	//Set the properties of the framework
        m_configMap = new StringMap(false);
        m_configMap.put(FelixConstants.LOG_LEVEL_PROP, String.valueOf(Logger.LOG_DEBUG));
        m_configMap.put("felix.fileinstall.dir", bundles.getAbsolutePath());
//        m_configMap.put("felix.auto.deploy.dir", bundles.getAbsolutePath());
//        m_configMap.put("felix.auto.deploy.action", "install, start");
//        m_configMap.put(DirectoryWatcher.POLL, "60000");	//No apparent effect
//        m_configMap.put(DirectoryWatcher.USE_START_TRANSIENT, "true");	//No apparent effect
        // Add core OSGi packages to be exported from the class path
        // via the system bundle.
        m_configMap.put(Constants.FRAMEWORK_SYSTEMPACKAGES, ANDROID_FRAMEWORK_PACKAGES);
    	m_configMap.put(BundleCache.CACHE_ROOTDIR_PROP, m_cache.getAbsolutePath());
    }
    
    private void startFelix(){
    	Resources res = getResources();
    	try {
    		List<BundleActivator> activators = new ArrayList<BundleActivator>();
    		// Manually install the basic bundles included in this App
    		activators.add(new DynInstaller(res,R.raw.osgicompendium,"file:///osgicompendium.jar"));
            activators.add(new DynInstaller(res,R.raw.orgapachefelixlog,"file:///orgapachefelixlog.jar"));
            activators.add(new DynInstaller(res,R.raw.orgapachefelixconfigadmin,"file:///orgapachefelixconfigadmin.jar"));
            activators.add(new DynInstaller(res,R.raw.fileinstall,"file:///fileinstall.jar"));
    		//Add the bundles to the properties and start Felix
    		m_configMap.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, activators);
    		m_felix = new Felix(m_configMap);
    		m_felix.start();
		} catch (BundleException ex) {
			throw new IllegalStateException(ex);
		}
    }
    
    @Override
    public synchronized void onStop() {
        System.out.println("============= ON STOP ==========");
    	super.onStop();
    	if(m_tracker!=null){
    		m_tracker.close();
    		m_tracker = null;
    	}
    	try {
			m_felix.stop();
		} catch (BundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	m_felix = null;
    }
    
    @Override
    public synchronized void onDestroy() {
    	super.onDestroy();
        System.out.println("============= ON DESTROY ==========");
    	delete(m_cache);
    	m_configMap = null;
    	m_cache = null;
    }
    
    private void delete(File target) {
    	if (target.isDirectory()) {
    		for (File file : target.listFiles()) {
    			delete(file);
    		}
    	}
    	target.delete();
    }
    
    private String getstate(int id){
    	switch (id) {
		case org.osgi.framework.Bundle.ACTIVE:
			return "ACTIVE";
		case org.osgi.framework.Bundle.INSTALLED:
			return "INSTALLED";
		case org.osgi.framework.Bundle.RESOLVED:
			return "RESOLVED";
		case org.osgi.framework.Bundle.STARTING:
			return "STARTING";
		case org.osgi.framework.Bundle.STOPPING:
			return "STOPPING";
		case org.osgi.framework.Bundle.UNINSTALLED:
			return "UNINSTALLED";
		default:
			return "UNKNOWN";
		}
    }
}