package org.universaal.android.FelixFileInstall.view;

import android.app.Activity;
import android.view.View;

public interface ViewFactory {
	public View create(Activity activity);
}
