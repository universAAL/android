package org.universaal.android.task35;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.universAAL.middleware.util.Constants;

public class TTAPropsManager {
	public static final String PROPS_FILE="tta.risk.properties";
	private static File confHome = new File(new File(Constants.getSpaceConfRoot()), "smp.tta.risk");
	
	public static final String TEXT="SMS.text";
	public static final String RISKTEXT="SMS.risk";
	public static final String NUMBER="SMS.number";
	public static final String SMSENABLE="SMS.enabled";
	public static final String DELAY="RISK.delay";
	public static final String DEFAULT="RISK.Room@Default";
	public static final String RISKENABLE="RISK.enabled";
	public static final String GPSTO="TTA.destination";
	protected static Properties properties = null;
	
	public static final String COMMENTS="This file stores persistence info for the Risk Manager stub. Times in minutes. \n" +
	"To set a risk situation timer for a room at a specific time, use the following: \n" +
	"RISK.Room@<URISuffixOfTheRoom>=00:<TimerMinutes>,<StartingHourOfPeriod>:<TimerMinutes>,... \n" +
	"Example: RISK.Room@Bathroom=00:60,06:150,12:60";
	
	public static synchronized Properties loadProperties(){
		if (properties == null) {
			properties = new Properties();
			try {
				properties = new Properties();
				InputStream in = new FileInputStream(new File(confHome, PROPS_FILE));
				properties.load(in);
				in.close();
			} catch (java.io.FileNotFoundException e) {
				System.out.println("Properties file does not exist; generating default...");
	
				properties.setProperty(TEXT,"Panic button pressed");
				properties.setProperty(RISKTEXT,"Risk situation detected");
				properties.setProperty(NUMBER,"123456789");
				properties.setProperty(SMSENABLE,"false");
				
				properties.setProperty(DELAY,"1");
				properties.setProperty(DEFAULT,"00:0");
				properties.setProperty(RISKENABLE,"false");
				
				properties.setProperty(GPSTO,"Rue Wiertz 60, 1047 Bruxelles, Belgique");
				saveProperties();
			}catch (Exception e) {
				System.out.println("Could not access properties file: " + e);
			}
		}
		return properties;
	}
    
    public static synchronized void saveProperties(){
    	if (properties == null)
    		return;
		try {
			//FileWriter out;
			//out = new FileWriter();
			OutputStream outStream = new FileOutputStream(new File(confHome, PROPS_FILE));
			properties.store(outStream, COMMENTS);
			outStream.close();
		} catch (Exception e) {
			System.out.println("Could not set properties file: " + e);
		}
	}
}
