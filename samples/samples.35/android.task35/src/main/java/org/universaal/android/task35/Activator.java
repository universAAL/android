package org.universaal.android.task35;

import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.universAAL.middleware.util.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class Activator implements BundleActivator{
	
	public static BundleContext context=null;
	
    public static ISubscriber rinput=null;
	public static OPublisher routput=null;
    public static GUICreator gui=null;

	public static final String PROPS_FILE="task35.properties";
	private static File confHome = new File(new File(Constants.getSpaceConfRoot()), "smp.task35");
	public static final String DELAY="form.delay";
	protected static Properties properties=new Properties();
	private ServiceCaller35 sCaller;
	private ServiceCallee35 sCallee;

	public static final String COMMENTS="Simple file to save same basics of the bundle for the 3.5 tools.";
	
	private final static Logger log=LoggerFactory.getLogger(Activator.class);
   
    public void start(BundleContext context) throws Exception {
    	log.info("Starting Risk manager stub bundle");
		properties=loadProperties();
		Activator.context=context;
		gui=new GUICreator();
		sCaller=new ServiceCaller35(context);
		sCallee=new ServiceCallee35(context);
		rinput=new ISubscriber(context);
		routput=new OPublisher(context);
		System.out.println("Started Task35 bundle");
    }


    public void stop(BundleContext context) {

    }
    
    public static synchronized Properties getProperties(){
		return properties;
	}
    
    
    private static synchronized Properties loadProperties(){
		Properties prop=new Properties();
		try {
			prop=new Properties();
			InputStream in = new FileInputStream(new File(confHome, PROPS_FILE));
			prop.load(in);
			in.close();
		} catch (java.io.FileNotFoundException e) {
			log.warn("Properties file does not exist; generating default...");

			prop.setProperty(DELAY,"30");
			
			setProperties(prop);
		}catch (Exception e) {
			log.error("Could not access properties file: {}",e);
		}
		return prop;
	}
    
    private static synchronized void setProperties(Properties prop){
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(new File(confHome, PROPS_FILE));
	    	BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
			prop.store(bufferedOutputStream, COMMENTS);
			bufferedOutputStream.close();
			fileOutputStream.close();
		} catch (Exception e) {
			log.error("Could not set properties file: {}",e);
		}
	}
}