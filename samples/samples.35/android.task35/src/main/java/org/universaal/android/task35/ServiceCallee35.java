package org.universaal.android.task35;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.universAAL.middleware.service.CallStatus;
import org.universAAL.middleware.service.ServiceCall;
import org.universAAL.middleware.service.ServiceCallee;
import org.universAAL.middleware.service.ServiceResponse;
import org.universAAL.middleware.service.owl.InitialServiceDialog;
import org.universAAL.middleware.service.owls.process.ProcessOutput;
import org.universAAL.middleware.service.owls.profile.ServiceProfile;
import org.universAAL.ontology.profile.User;


public class ServiceCallee35 extends ServiceCallee{
	private static final ServiceResponse failure = new ServiceResponse(CallStatus.serviceSpecificFailure);
	private final static Logger log=LoggerFactory.getLogger(ServiceCallee35.class);
	
	private final static String NAMESPACE_CALLEE35 = "http://ontology.uaal.org/t35.owl#";
	public final static String DIALOG_HOME_SCREEN = NAMESPACE_CALLEE35 + "HomeScreen";
	public final static String DIALOG_CONFIG_SCREEN = NAMESPACE_CALLEE35 + "TTAConfigScreen";
	private final static String START_INSTALL_SCREEN = NAMESPACE_CALLEE35 + "startUIhome";
	private final static String START_CONFIG_SCREEN = NAMESPACE_CALLEE35 + "startTTAConfig";

	public ServiceCallee35(BundleContext context){
		super(context, getProfiles());
	}
	
	protected ServiceCallee35(BundleContext context, ServiceProfile[] realizedServices) {
		super(context, realizedServices);
		System.out.println("Registered the ServiceCallee of Task 35");
	}

	public void communicationChannelBroken() {
		
	}

	public ServiceResponse handleCall(ServiceCall call) {
		System.out.println("Received call");
		User user=null;
		if (call == null){
			failure.addOutput(new ProcessOutput(ServiceResponse.PROP_SERVICE_SPECIFIC_ERROR,"Null call!?!"));
			log.error("Null call");
			return failure;
		}
		
		String operation = call.getProcessURI();
		if (operation == null){
			failure.addOutput(new ProcessOutput(ServiceResponse.PROP_SERVICE_SPECIFIC_ERROR,"Null operation!?!"));
			log.error("Null op");
			return failure;
		}
		
		if (operation.startsWith(NAMESPACE_CALLEE35)){
			log.debug("Start UI op");
			Object inputUser = call.getInvolvedUser(); 
			if ((inputUser == null)||!(inputUser instanceof User)){
				failure.addOutput(new ProcessOutput(ServiceResponse.PROP_SERVICE_SPECIFIC_ERROR,"Invalid User Input!"));
				log.debug("No user");
				return failure;
			}else{
				user=(User)inputUser;
			}
			log.debug("Show dialog from call");
			ServiceResponse response=failure;
			if (operation.startsWith(START_INSTALL_SCREEN)){
				Activator.routput.showInstallForm(user);
				response= new ServiceResponse(CallStatus.succeeded);
			} else if (operation.startsWith(START_CONFIG_SCREEN)){
				Activator.routput.showTTAScreenForm(user);
				response= new ServiceResponse(CallStatus.succeeded);
			}
			return response;
		}
		System.out.println("finished");
		return null;
	}

	static ServiceProfile[] getProfiles() {
		System.out.println("Create Service Profiles of Task35");
		ServiceProfile prof1 = InitialServiceDialog.createInitialDialogProfile(
				DIALOG_HOME_SCREEN,
				"http://www.fraunhofer-igd.de", "Homescreen of Task3.5 tools",
				START_INSTALL_SCREEN);
		ServiceProfile prof2 = InitialServiceDialog.createInitialDialogProfile(
				DIALOG_CONFIG_SCREEN,
				"http://www.fraunhofer-igd.de", "Configuration Screen for TTA App",
				START_CONFIG_SCREEN);
		return new ServiceProfile[]{prof1, prof2};
	}




}
