package org.universaal.android.task35;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.universAAL.middleware.input.InputEvent;
import org.universAAL.middleware.input.InputSubscriber;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.middleware.util.Constants;
import org.universAAL.ontology.profile.User;

/**
 *
 * @author amarinc
 */
public class ISubscriber extends InputSubscriber{
	
	private final static Logger log=LoggerFactory.getLogger(ISubscriber.class);
	public final static String AUTO_START_FOLDER = Constants.getSpaceConfRoot()+"/auto/";
	
	// helping variable for saving the file to install
	String installFilePath = null;
	boolean agreedToLicense = false;
	
	protected ISubscriber(BundleContext context) {
		super(context);
	}

	public void communicationChannelBroken() {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see org.persona.middleware.input.InputSubscriber#handleInputEvent(org.persona.middleware.input.InputEvent)
	 */
	public void handleInputEvent(InputEvent event) {
		User user=(User) event.getUser();
		System.out.println("Received an Input Event from user " + user.getURI());
		String submit=event.getSubmissionID();
		Resource u = event.getUser();

		try{
			if(	submit.equals(GUICreator.SUBMIT_INSTALL_CANCEL) ||
				submit.equals(GUICreator.SUBMIT_LICENSE_CANCEL) ||
				submit.equals(GUICreator.SUBMIT_CONFIG_CANCEL)){
				System.out.println("Input received was Cancel");
				installFilePath = null;
				agreedToLicense = false;
				
			} else if (submit.equals(GUICreator.SUBMIT_INSTALL)) {
				
				String manualInstall = null;
				String autoInstall = null;
				if (event.getUserInput(new String[]{(GUICreator.INPUT_INSTALL_FILE)}) != null)
					manualInstall = event.getUserInput(new String[]{(GUICreator.INPUT_INSTALL_FILE)}).toString();
				if (event.getUserInput(new String[]{(GUICreator.INPUT_INSTALL_GIVEN_FILE)}) != null)
					autoInstall = event.getUserInput(new String[]{(GUICreator.INPUT_INSTALL_GIVEN_FILE)}).toString();
				
				System.out.println("Received input auto: " + autoInstall);
				System.out.println("Received input manual: " + manualInstall);
		
				if (manualInstall != null && !manualInstall.equals(""))
					installFilePath = manualInstall;
				else if (autoInstall != null && !autoInstall.equals(""))
					installFilePath = GUICreator.DOWNLOAD_PATH + "/" + autoInstall;
				
				if (installFilePath != null && !installFilePath.equals(""))
					Activator.routput.showLicenseForm(user, installFilePath);
				
			} else if (submit.equals(GUICreator.SUBMIT_LICENSE)) {
				agreedToLicense = true;
				if (installFilePath != null && !installFilePath.equals(""))
					Activator.routput.showTTAScreenForm(user);
			} else if (submit.equals(GUICreator.SUBMIT_CONFIG)) {
				
				System.out.println("Received Config Input:");
				String adress = event.getUserInput(new String[]{(GUICreator.INPUT_CONFIG_ADRESS)}).toString();
				String phone = event.getUserInput(new String[]{(GUICreator.INPUT_CONFIG_PHONE)}).toString();
				String use_sms = event.getUserInput(new String[]{(GUICreator.INPUT_CONFIG_USE_SMS)}).toString();
				String sms_text = event.getUserInput(new String[]{(GUICreator.INPUT_CONFIG_SMS_TEXT)}).toString();
				System.out.println(adress);
				System.out.println(phone);
				System.out.println(use_sms);
				System.out.println(sms_text);
				
				Properties properties = TTAPropsManager.loadProperties();
				
				properties.setProperty(TTAPropsManager.TEXT,sms_text);
				properties.setProperty(TTAPropsManager.NUMBER,phone);
				if (use_sms.contains("No"))
					properties.setProperty(TTAPropsManager.SMSENABLE,"false");
				else
					properties.setProperty(TTAPropsManager.SMSENABLE,"true");				
				properties.setProperty(TTAPropsManager.GPSTO,adress);
				
				TTAPropsManager.saveProperties();
				
				if (agreedToLicense) {
					installApp(u);
					installFilePath = null;
					agreedToLicense = false;
				}
			}
		}catch(Exception e){
			log.error("Error while processing the user input: {}",e);
		}
	}
	
	private void installApp(Resource u) {
		if (installFilePath != null && !installFilePath.equals("")) {
			File installFile = new File(installFilePath);
			if (installFile.isFile()) {
				File file = new File(AUTO_START_FOLDER, installFile.getName());
				boolean success = installFile.renameTo(file);
				if (success) {
					System.out.println("File \""+installFilePath+"\" installed!");
					addEntryToMenue("Config TTA","http://www.fraunhofer-igd.de",ServiceCallee35.DIALOG_CONFIG_SCREEN);
					addEntryToMenue("Panic Button","http://www.tsb.upv.es","http://ontology.aal-persona.org/tta.owl#Panic");
					addEntryToMenue("Take me Home","http://www.tsb.upv.es","http://ontology.aal-persona.org/tta.owl#TakeHome");
				} else
					System.out.println("File \""+installFilePath+"\" not installed!");
			}
			else
				System.out.println("File \""+installFilePath+"\" not found!");
		} else
			System.out.println("Install input not valid!");
	}

	public void dialogAborted(String dialogID) {
		// TODO Auto-generated method stub
		
	}
	
	public void subscribe(String dialogID) {
		addNewRegParams(dialogID);
	}
	
	public void addEntryToMenue(String caption, String provider, String call) {
		String lang = Locale.getDefault().getLanguage();
		appendEntryToMenue(lang, caption, provider, call);
	}
	
	public void appendEntryToMenue(String language, String caption, String provider, String call) {
		List<String> lines = new ArrayList<String>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(Constants
				    .getSpaceConfRoot()+"/ui.dm", "main_menu_"+"saied_"+Locale.getDefault().getLanguage()+".txt")));
			String line;
			while ( (line = in.readLine()) != null)
				lines.add(line);
			in.close();
			
			String newLine = "/"+caption+"|"+provider+"|"+call;
			if (lines.contains(newLine))
				return;
			
			BufferedWriter out = new BufferedWriter(new FileWriter(new File(Constants
				    .getSpaceConfRoot()+"/ui.dm", "main_menu_"+"saied_"+Locale.getDefault().getLanguage()+".txt")));
			for (int index = 0; index<lines.size(); index++) {
				if (index == lines.size()-1)
					out.write(newLine+System.getProperty("line.separator"));
				out.write(lines.get(index)+System.getProperty("line.separator"));
			}
			out.close();
			
			System.out.println("Main-menu file \""+"main_menu_"+"saied_"+Locale.getDefault().getLanguage()+".txt"+"\" has been appended!");
		}
		catch (IOException e) {
			System.out.println("File not found: " + "main_menu_"+"saied_"+Locale.getDefault().getLanguage()+".txt");
			e.printStackTrace();
		}
	}
}
