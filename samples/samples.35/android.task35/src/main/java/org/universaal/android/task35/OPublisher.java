package org.universaal.android.task35;

import java.util.Locale;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.universAAL.middleware.io.owl.PrivacyLevel;
import org.universAAL.middleware.io.rdf.Form;
import org.universAAL.middleware.output.OutputEvent;
import org.universAAL.middleware.output.OutputPublisher;
import org.universAAL.middleware.owl.supply.LevelRating;
import org.universAAL.ontology.profile.User;

/**
 *
 * @author amarinc
 */
public class OPublisher extends OutputPublisher {
	
	private final static Logger log=LoggerFactory.getLogger(OPublisher.class);
	
	protected OPublisher(BundleContext context) {
		super(context);
	}

	public void communicationChannelBroken() {
		// TODO Auto-generated method stub
		
	}
	
	public void showInstallForm(User user) {
		log.debug("Show install screen");
		Form f = Activator.gui.getInstallForm();
		OutputEvent oe = new OutputEvent(user,f,LevelRating.high,Locale.ENGLISH,PrivacyLevel.insensible);
		Activator.rinput.subscribe(f.getDialogID());
		publish(oe);
	}
	
	public void showTTAScreenForm(User user) {
		log.debug("Show TTA Config-Screen");
		Form f = Activator.gui.getTTAForm();
		OutputEvent oe = new OutputEvent(user,f,LevelRating.high,Locale.ENGLISH,PrivacyLevel.insensible);
		Activator.rinput.subscribe(f.getDialogID());
		publish(oe);
	}
	
	public void showLicenseForm(User user, String pathToInstallJar) {
		log.debug("Show License Screen");
		Form f = Activator.gui.getLicenseForm(pathToInstallJar);
		OutputEvent oe = new OutputEvent(user,f,LevelRating.high,Locale.ENGLISH,PrivacyLevel.insensible);
		Activator.rinput.subscribe(f.getDialogID());
		publish(oe);
	}
}
