package org.universaal.android.task35;

import org.osgi.framework.BundleContext;
import org.universAAL.middleware.service.DefaultServiceCaller;


/**
 *
 * @author amarinc
 */
public class ServiceCaller35 {
	DefaultServiceCaller caller;
	public static final String TOOLS35_MANAGEMENT_NAMESPACE= "http://ontology.uaal.igd.de/Tools35.owl#";
	public static final String OUTPUT_VC = TOOLS35_MANAGEMENT_NAMESPACE + "VCOutput";
	
    protected ServiceCaller35(BundleContext context) {
    	caller=new DefaultServiceCaller(context);
	}

}
