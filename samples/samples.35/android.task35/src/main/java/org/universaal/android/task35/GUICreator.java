package org.universaal.android.task35;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.universAAL.middleware.io.rdf.Form;
import org.universAAL.middleware.io.rdf.Group;
import org.universAAL.middleware.io.rdf.InputField;
import org.universAAL.middleware.io.rdf.Label;
import org.universAAL.middleware.io.rdf.Select;
import org.universAAL.middleware.io.rdf.Select1;
import org.universAAL.middleware.io.rdf.SimpleOutput;
import org.universAAL.middleware.io.rdf.Submit;
import org.universAAL.middleware.io.rdf.TextArea;
import org.universAAL.middleware.rdf.PropertyPath;
import org.universAAL.middleware.util.Constants;


public class GUICreator{
	public static final String PERSONA_INPUT_NAMESPACE = "http://ontology.uaal.org/Input.owl#"; //$NON-NLS-1$
	
	public static final String SUBMIT_INSTALL = PERSONA_INPUT_NAMESPACE + "installApp"; //$NON-NLS-1$
	public static final String SUBMIT_INSTALL_CANCEL = PERSONA_INPUT_NAMESPACE + "cancelInstall"; //$NON-NLS-1$
	public static final String SUBMIT_CONFIG = PERSONA_INPUT_NAMESPACE + "configApp"; //$NON-NLS-1$
	public static final String SUBMIT_CONFIG_CANCEL = PERSONA_INPUT_NAMESPACE + "cancelConfig"; //$NON-NLS-1$
	public static final String SUBMIT_LICENSE = PERSONA_INPUT_NAMESPACE + "licenseApp"; //$NON-NLS-1$
	public static final String SUBMIT_LICENSE_CANCEL = PERSONA_INPUT_NAMESPACE + "cancelLicense"; //$NON-NLS-1$
	
	public static final String INPUT_INSTALL_FILE = PERSONA_INPUT_NAMESPACE + "inputInstallFile"; //$NON-NLS-1$
	public static final String INPUT_INSTALL_GIVEN_FILE = PERSONA_INPUT_NAMESPACE + "inputInstallGivenFile"; //$NON-NLS-1$
	
	public static final String INPUT_CONFIG_ADRESS = PERSONA_INPUT_NAMESPACE + "inputConfigAdress"; //$NON-NLS-1$
	public static final String INPUT_CONFIG_PHONE = PERSONA_INPUT_NAMESPACE + "inputConfigPhone"; //$NON-NLS-1$
	public static final String INPUT_CONFIG_USE_SMS = PERSONA_INPUT_NAMESPACE + "inputConfigUseSMS"; //$NON-NLS-1$
	public static final String INPUT_CONFIG_SMS_TEXT = PERSONA_INPUT_NAMESPACE + "inputConfigSMSText"; //$NON-NLS-1$
	
	public static final String INSTALL_TITLE = Messages.getString("Task35Gui.1"); //$NON-NLS-1$
	public static final String INSTALL_OK = Messages.getString("Task35Gui.2"); //$NON-NLS-1$
	public static final String INSTALL_CANCEL = Messages.getString("Task35Gui.3"); //$NON-NLS-1$

	private static final String imgroot="android.handler/"; //$NON-NLS-1$ //$NON-NLS-2$
	
	private final static Logger log=LoggerFactory.getLogger(GUICreator.class);
	
	public final static String DOWNLOAD_PATH = Constants.getSpaceConfRoot() + "/downloads";
	//public final static String DOWNLOAD_PATH = "/sdcard/downloads";
	
	public Form getInstallForm() {
		log.debug("Generating install form");
		System.out.println("Check for files in: " +DOWNLOAD_PATH);
		Form f = Form.newDialog(INSTALL_TITLE, (String)null);
		Group controls = f.getIOControls();
		Group submits = f.getSubmits();
		
		File downloadFolder = new File(DOWNLOAD_PATH);
        if (downloadFolder.isDirectory()) {
        	List<String> files = new ArrayList<String>();
        	for (File file : downloadFolder.listFiles()) {
        		String filename = file.getName();
        		if (filename.endsWith(".jar"))
        			files.add(filename);
        	}
        	Select1 s1=new Select1(controls,new Label("Choose a file to install or give the full path:",
        			(String)null),new PropertyPath(null, false, new String[]{INPUT_INSTALL_GIVEN_FILE}),null,null);
    		s1.generateChoices(files.toArray());
        }
        else {
        	new SimpleOutput(controls,null,null,"No new apps found!");
        }
		
		new InputField(controls,null,new PropertyPath(null,false,new String[]{INPUT_INSTALL_FILE}),null,"");
		
		Label labelBoton = new Label(INSTALL_OK,imgroot + "ShinyButtonGreen5.jpg");
		new Submit(submits,labelBoton,SUBMIT_INSTALL);
		labelBoton = new Label(INSTALL_CANCEL,imgroot + "ShinyButtonGreen5.jpg");
		new Submit(submits,labelBoton, SUBMIT_INSTALL_CANCEL);
		
		return f;
	}
	
	public Form getTTAForm() {
		log.debug("Generating config form");
		Form f = Form.newDialog(INSTALL_TITLE, (String)null);
		Group controls = f.getIOControls();
		Group submits = f.getSubmits();
		
		Properties properties = TTAPropsManager.loadProperties();
		String smsText = properties.getProperty(TTAPropsManager.TEXT);
		String phoneNumer = properties.getProperty(TTAPropsManager.NUMBER);
		String sendSMS = properties.getProperty(TTAPropsManager.SMSENABLE);			
		String adress = properties.getProperty(TTAPropsManager.GPSTO);
		
		new InputField(controls,new Label("Your Adress:",(String)null),new PropertyPath(null,false,new String[]{INPUT_CONFIG_ADRESS}),null,adress != null ? adress : "");
		new InputField(controls,new Label("Your Phone-Number:",(String)null),new PropertyPath(null,false,new String[]{INPUT_CONFIG_PHONE}),null,phoneNumer != null ? phoneNumer : "");
		Select1 ms1=new Select1(controls,new Label("Enable SMS Messages:",(String)null),new PropertyPath(null, false, new String[]{INPUT_CONFIG_USE_SMS}),null,sendSMS != null && sendSMS.contains("false")? "No" : "Yes");
		ms1.generateChoices(new String[]{"Yes", "No"});
		new TextArea(controls,new Label("SMS message text (optional)",(String)null),new PropertyPath(null,false,new String[]{INPUT_CONFIG_SMS_TEXT}),null,smsText != null ? smsText : "");
		
		Label labelBoton = new Label(INSTALL_OK,imgroot + "ShinyButtonGreen5.jpg");
		new Submit(submits,labelBoton,SUBMIT_CONFIG);
		labelBoton = new Label(INSTALL_CANCEL,imgroot + "ShinyButtonGreen5.jpg");
		new Submit(submits,labelBoton,SUBMIT_CONFIG_CANCEL);
		
		return f;
	}
	
	public Form getLicenseForm(String pathToInstallJar) {
		log.debug("Generating license form");
		Form f = Form.newDialog(INSTALL_TITLE, (String)null);
		Group controls = f.getIOControls();
		Group submits = f.getSubmits();
		
		if (pathToInstallJar.endsWith(".jar")) {
			String license = readOutFile(pathToInstallJar.substring(0, pathToInstallJar.length()-3)+"txt");
			new SimpleOutput(controls,new Label("Please agree to license agreement:",(String)null),null,license);
			
			Label labelBoton = new Label(INSTALL_OK,imgroot + "ShinyButtonGreen5.jpg");
			new Submit(submits,labelBoton,SUBMIT_LICENSE);
		} else
			new SimpleOutput(controls,null,null,"Could not find a valid license agreemnt!");
		
		Label labelBoton = new Label(INSTALL_CANCEL,imgroot + "ShinyButtonGreen5.jpg");
		new Submit(submits,labelBoton,SUBMIT_LICENSE_CANCEL);
		
		return f;
	}
	
	private String readOutFile(String pathToFile) {
		try{
			File f = new File(pathToFile);
			FileInputStream fileIS = new FileInputStream(f);
			BufferedReader buf = new BufferedReader(new InputStreamReader(fileIS));
			String readString = new String();
			StringBuffer output = new StringBuffer();
			while((readString = buf.readLine())!= null) {
				output.append(readString + "\n");
			}
			fileIS.close();
			return output.toString();
		} catch (Exception e) {
			return "Could't not read file \"" + pathToFile + "\"";
		} 
    }
	
}