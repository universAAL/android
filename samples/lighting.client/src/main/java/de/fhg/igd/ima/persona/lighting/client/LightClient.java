package de.fhg.igd.ima.persona.lighting.client;

import org.persona.ontology.Location;
import org.persona.platform.casf.ontology.device.Device;

import de.fhg.igd.ima.persona.location.FHLocation;

public class LightClient {
	
	private String[] lamps = null;

	static {

		// force the JVM to load the class location classes
		FHLocation.getClassRestrictionsOnProperty(null);
		Location.getClassRestrictionsOnProperty(null);
	}
	
	public LightClient() {	
		Device[] d = LightingConsumer.getControlledLamps();
		lamps = new String[d.length];
		
		for(int i =0; i < d.length; i++){
			lamps[i] = d[i].getURI();
		}
		
		System.out.println("Lighting-Client started with "+lamps.length+" lights!");
	}

	private boolean getOn(int i) {
		if(i >= 0 && i < lamps.length) {
			LightingConsumer.turnOn(lamps[i]);
			return true;
		}
		return false;
	}
	
	private boolean getOff(int i) {
		if(i >= 0 && i < lamps.length) {
			LightingConsumer.turnOff(lamps[i]);
			return true;
		}
		return false;
	}
}
