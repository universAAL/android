/*
	Copyright 2011 ITACA-TSB, http://www.tsb.upv.es
	Instituto Tecnologico de Aplicaciones de Comunicacion 
	Avanzadas - Grupo Tecnologias para la Salud y el 
	Bienestar (TSB)
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universAAL.android.felix;

import org.universAAL.android.felix.FelixService.OSGiBinder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.os.IBinder;

public class FelixActivity extends Activity{
	
	private ServiceConnection m_Connection = new OSGiConnection();
	private boolean m_bound=false;
	private ComponentName m_serviceName=null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Splash screen with loading animation
        setSplashScreen(R.string.splash_starting);
        //Start the service
        Intent intent = new Intent("org.universAAL.android.felix.REMOTE_SERVICE");
		m_serviceName=startService(intent);
    }
    
	@Override
	protected void onStart() {
		super.onStart();
		Intent intent = new Intent("org.universAAL.android.felix.REMOTE_SERVICE");
		m_bound=bindService(intent, m_Connection, 0);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(m_bound && m_serviceName!=null)
			unbindService(m_Connection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Create options menu
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.optmenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
		case R.id.startstop:
			unbindService(m_Connection);//TODO: If bound?
			Intent intent = new Intent("org.universAAL.android.felix.REMOTE_SERVICE");
			if(stopService(intent))m_serviceName=null;
			this.finish();
			return true;
		default: return super.onOptionsItemSelected(item);
		}
	}
	
	private void setSplashScreen(int textResourceID){
		// Handles app own GUI, which is just a splash screen until service takes it
		setContentView(R.layout.splash);
        TextView loadtext=(TextView)findViewById(R.id.textView1);
        loadtext.setText(textResourceID);
        Animation anim=AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);
        loadtext.startAnimation(anim);
	}
	
	//---------------Classes--------------------
	
	protected class OSGiConnection implements ServiceConnection{
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// Send this activity to service through binder, so it puts the GUI
			OSGiBinder binder = (OSGiBinder) service;
			binder.linkActivity(FelixActivity.this);
		}
		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			//Reset the default GUI
			setSplashScreen(R.string.splash_disconnected);
		}
	}
	

}
