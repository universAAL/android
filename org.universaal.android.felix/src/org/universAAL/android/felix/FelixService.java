/*
	Copyright 2011 ITACA-TSB, http://www.tsb.upv.es
	Instituto Tecnologico de Aplicaciones de Comunicacion 
	Avanzadas - Grupo Tecnologias para la Salud y el 
	Bienestar (TSB)
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universAAL.android.felix;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.felix.framework.FrameworkFactory;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.launch.Framework;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

//import ae.com.sun.xml.bind.v2.model.annotation.RuntimeInlineAnnotationReader;
//import ae.com.sun.xml.bind.v2.model.annotation.XmlSchemaMine;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class FelixService extends Service{

	private static final String CONF_FILE = "/data/felix/configurations/config.properties";
	private static final String CACHE_FOLDER = "/data/felix/cache";
	private static final int ONGOING_NOTIFICATION = 13879;
	private Properties m_configProps;
	private Framework m_fwk;
	private MulticastLock m_lock;
	private IActivityStub m_iohandler=null;
	private Activity m_activityHandle=null;
	private ServiceTracker m_tracker;
	private boolean m_felixrunning=false;
	private OSGiBinder m_Binder=new OSGiBinder();
	private IContextStub m_context=new ContextStubImpl();
	
	@Override
	public void onCreate() {
		Log.d("FelixService", "Creating Service");
		// Load the Felix and OSGi properties
		m_configProps = new Properties();
		try { // TODO monitor mounted media, and auto generate blank file if not present
			File conf = new File(Environment
					.getExternalStorageDirectory().getPath(), CONF_FILE);
			if (!conf.exists()) {
				Toast.makeText(this,
						"No properties file found, framework will be empty\n "
								+ "Put a config.properties file in "
								+ conf.getParent(), Toast.LENGTH_LONG).show();
				Log.w("FelixService", "No properties file found.");
			} else {
				InputStream in = new FileInputStream(conf);
				m_configProps.load(in);
				in.close();
			}
			// Copy system properties in file to actual system properties
			Properties sysProps = System.getProperties();
			sysProps.putAll(m_configProps);
			System.setProperties(sysProps);
			// Make sure cache folder is clear and set it as felix cache
			File cache = new File(Environment
					.getExternalStorageDirectory().getPath(), CACHE_FOLDER);
			delete(cache);
			if (!cache.mkdirs()) {
				throw new IllegalStateException("Unable to create cache folder");
			}
			if (cache != null) {
				m_configProps.setProperty(Constants.FRAMEWORK_STORAGE,cache.getAbsolutePath());
			}
			// Create instance of framework
			FrameworkFactory factory = FrameworkFactory.class.newInstance();
			m_fwk = factory.newFramework(m_configProps);
		} catch (Exception e) {
			Toast.makeText(this, "Error starting Service. Felix is not running", Toast.LENGTH_LONG).show();
			Log.e("FelixService", e.toString());
			e.printStackTrace();
			return;
		}
		// Create notification to be used to access the GUI
		Notification notif = new Notification(R.drawable.ic_stat_notify,
				getText(R.string.notif_text), System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, FelixActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notif.setLatestEventInfo(this, getText(R.string.notif_title),
				getText(R.string.notif_message), pendingIntent);
		// Make foreground to run forever... ? here?
		startForeground(ONGOING_NOTIFICATION, notif);
		// Setup wifi multicast to connect other nodes
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		m_lock = wifi.createMulticastLock("felix_multicast");
		m_lock.acquire();
		// Start Felix in a thread because Activity holds until onCreate returns
		// Would make sense in onStart, but that is called in every screen reorientation
		Thread thr = new Thread(new FelixRunner());
		thr.setPriority(Thread.MAX_PRIORITY);//TODO: safe? battery? Remove if remote process?
		thr.start();
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d("FelixService","Binding Service");
		return m_Binder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d("FelixService","Unbinding Service");
		m_activityHandle=null;
		updateUIlink();
		return false;
	}

	@Override
	public void onDestroy() {
		Log.d("FelixService","Stopping Service");
		// TODO: UNRegister for OSGI services implementing Android UIs?
		try {
			m_fwk.stop();
			m_felixrunning=false;
		} catch (BundleException e) {
			e.printStackTrace();
		}
		// Free resources and quit service from foreground: it can be auto killed
		m_fwk = null;
		File cache = new File(Environment
				.getExternalStorageDirectory().getPath(), CACHE_FOLDER);
		delete(cache);
		m_lock.release();
		stopForeground(true);
	}
	
	/**
	 * Links/unlinks GUI of Activity to IO Handler, if any.
	 */
	private void updateUIlink(){
		Log.d("FelixService","Updating UI link");
		if (m_iohandler != null && m_activityHandle!=null) {
			Log.d("FelixService","Updating UI link: We have HANDLER and ACTIVITY");
			m_activityHandle.runOnUiThread(new Runnable() {//TODO: no run on UI?
				public void run() {
					m_activityHandle.setContentView(m_iohandler.buildViewFromActivity(m_activityHandle));
				}
			});
		}else if(m_iohandler == null && m_activityHandle!=null){
			Log.d("FelixService","Updating UI link: We don�t have HANDLER but have ACTIVITY");
			m_activityHandle.runOnUiThread(new Runnable() {//TODO: no run on UI?
				public void run() {
					m_activityHandle.setContentView(R.layout.splash);
					TextView loadtext=(TextView)m_activityHandle.findViewById(R.id.textView1);
					// If m_felixrunning is true it means the handler left or not come yet.
					loadtext.setText(m_felixrunning?R.string.splash_waitui:R.string.splash_loading);
					Animation anim=AnimationUtils.loadAnimation(m_activityHandle, android.R.anim.fade_in);
			        anim.setRepeatCount(Animation.INFINITE);
			        anim.setDuration(3000);
			        loadtext.startAnimation(anim);
				}
			});
		}else if(m_iohandler != null && m_activityHandle==null){
			Log.d("FelixService","Updating UI link: We have HANDLER but don�t have ACTIVITY");
			m_iohandler.buildViewFromActivity(m_activityHandle);
		}
	}
	
	/**
	 * Recursive method to wipe out cache
	 * 
	 * @param target
	 *            Folder to remove
	 */
	private void delete(File target) {
		if (target.isDirectory()) {
			for (File file : target.listFiles()) {
				delete(file);
			}
		}
		target.delete();
	}
	
	//---------------Classes--------------------
	
	//TODO: Use IPC handlers so that service runs in different process : faster?
	public class OSGiBinder extends Binder {
		void linkActivity(final Activity act) {//TODO: FINAL??????
			// Got the activity. Let�s see if we have a IO handler to link it to.
			m_activityHandle=act;
			updateUIlink();
		}
	}
	
	// Official implementation of IContextStub
	public class ContextStubImpl implements IContextStub{
		@Override
		public Context getAndroidContext() {
			//Return this service�s context
			return FelixService.this; //TODO .getbaseContext??? will affect handler?
		}
	}
	
	protected class FelixRunner implements Runnable{
		@Override
		public void run() {
			Log.d("FelixRunner","Trying to run Felix");
			//This is for uAAL MW 2.0. See http://www.docx4java.org/blog/2012/05/jaxb-can-be-made-to-run-on-android/
//			RuntimeInlineAnnotationReader.cachePackageAnnotation(org.universAAL.middleware.connectors.deploy.karaf.model.ObjectFactory.class.getPackage(), 
//					new XmlSchemaMine("http://karaf.apache.org/xmlns/features/v1.0.0"));
//			RuntimeInlineAnnotationReader.cachePackageAnnotation(org.universAAL.middleware.deploymanager.uapp.model.ObjectFactory.class.getPackage(), 
//					new XmlSchemaMine("http://www.universaal.org/aal-uapp/v1.0.0"));
//			RuntimeInlineAnnotationReader.cachePackageAnnotation(org.universAAL.middleware.interfaces.aalspace.model.ObjectFactory.class.getPackage(), 
//					new XmlSchemaMine("http://universaal.org/aalspace-channel/v1.0.0"));
			// Add the following jar files to libs: 
//			activation.jar(javax.activation...); 
//			additional.jar(java.awt.datatransfer...);  
//			istack-commons-runtime(com.sun.istack.Builder..); 
//			jaxb-android-2.2.5.jar(ae.com.sun.xml.bind...);
//			mw.schemas-2.X.X;
//			txw2-20110809.jar(com.sun.xml.txw2...);
			try {
				// Start felix
				m_fwk.init();
				m_fwk.start();
				// Install & start bundles from the conf file
				AutoProcessor.process(m_configProps, m_fwk
						.getBundleContext());
				// Register for OSGI services implementing Android UIs
				m_tracker = new ServiceTracker(m_fwk.getBundleContext(), 
						m_fwk.getBundleContext().createFilter(
								"(" + Constants.OBJECTCLASS + "="
										+ IActivityStub.class.getName() + ")"),
						new ActivityStubTracker());
				m_tracker.open();
				//publish in OSGi the android context, for the bundles that want to use it without UI
				m_fwk.getBundleContext().registerService(IContextStub.class.getName(), m_context, null);
			} catch (Exception e) {
				if (m_activityHandle != null)
					m_activityHandle.runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(m_activityHandle,
									"Error running Service. Felix may not be running",
									Toast.LENGTH_LONG).show();
						}
					});
				Log.e("FelixRunner","Could not create framework: " + e);
				e.printStackTrace();
			}
			
			//When autoprocessor is done starting bundles and all is up, change splash
			if(m_activityHandle!=null)m_activityHandle.runOnUiThread(new Runnable() {
				public void run() {
					m_activityHandle.setContentView(R.layout.splash);
			        TextView loadtext=(TextView)m_activityHandle.findViewById(R.id.textView1);
			        //m_activityHandle may be in hands of a IOhandler, and textView1 not present
			        if(loadtext!=null)loadtext.setText(R.string.splash_waitui);
			        Animation anim=AnimationUtils.loadAnimation(m_activityHandle, android.R.anim.fade_in);
			        anim.setRepeatCount(Animation.INFINITE);
			        anim.setDuration(3000);
			        loadtext.startAnimation(anim);
				}
			});
			// Only used for UI small tricks, not important
			m_felixrunning=true;
		}
	}
	
	// Updates UI link when new handlers arrive (or leave)
	protected class ActivityStubTracker implements ServiceTrackerCustomizer{
		@Override
		public Object addingService(ServiceReference ref) {
			Log.d("ActivityStubTracker","Found a UI implementor");
			m_iohandler = (IActivityStub) m_fwk.getBundleContext().getService(ref);
			updateUIlink();
			return m_iohandler;//TODO: manage a pile of IActivityStub
		}
		@Override
		public void modifiedService(ServiceReference ref, Object service) {
			Log.d("ActivityStubTracker","Found changed UI implementor");
			removedService(ref, service);
			addingService(ref);
			updateUIlink();
		}
		@Override
		public void removedService(ServiceReference ref, Object service) {
			Log.d("ActivityStubTracker","Removed a UI implementor");
			m_fwk.getBundleContext().ungetService(ref);
			m_iohandler=null;//TODO: manage a pile of IActivityStub
			updateUIlink();
		}
	}
}
