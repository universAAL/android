After update 17 to android eclipse plugin, there is a change in the dependency management that may affect your IDE.
It may happen that you "run as android app" the application and this doesn�t work because it doesn�t find some felix class.
Currently this code is adapted to work with revision 17 and later, but previous eclipse plugins may have problems.
If so, you can revert to the previous state by renaming the "libs" folder to "lib" and making sure the felix jar in there
is properly referenced as "add Jar" in the "java build path > libraries" of the project.

After update 22 to android eclipse plugin, these stubborn googlers have chagned yet again the way dependencies are managed.
It may happen that you "run as android app" the application and this doesn�t work because it doesn�t find some felix class.
If this happens check right-clicking the project, then go to the Build path > Order & Export path.
The "Android Private Libraries" should be checked, but the error may still appear after that.
That�s because Ecilpse hasn�t noticed. Try every refreshing technique (F5, close/open, restart...). If that is not enough,
try changing the Order of those libraries in the Order & Export.
This should be fixed in a later android tools update, but just in case...