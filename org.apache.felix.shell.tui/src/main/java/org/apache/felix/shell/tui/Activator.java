/* 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.felix.shell.tui;

import java.io.*;

import org.apache.felix.shell.ShellService;
import org.osgi.framework.*;
import org.universaal.android.FelixConfProp.view.ViewFactory;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Activator implements BundleActivator{
    private BundleContext m_context = null;
    private ServiceReference m_shellRef = null;
    private ShellService m_shell = null;
	private AndroidViewprovider m_Factory;

    public void start(BundleContext context)
    {
        m_context = context;
        //Instance the ViewFactory
        m_Factory=new AndroidViewprovider();

        // Listen for registering/unregistering impl service.
        ServiceListener sl = new ServiceListener() {
            public void serviceChanged(ServiceEvent event)
            {
                synchronized (Activator.this)
                {
                    // Initialize the service if we don't have one.
                    if ((event.getType() == ServiceEvent.REGISTERED)
                        && (m_shellRef == null))
                    {
                        initializeService();
                    }
                    // Unget the service if it is unregistering.
                    else if ((event.getType() == ServiceEvent.UNREGISTERING)
                        && event.getServiceReference().equals(m_shellRef))
                    {
                        m_context.ungetService(m_shellRef);
                        m_shellRef = null;
                        m_shell = null;
                        // Try to get another service.
                        initializeService();
                    }
                }
            }
        };
        try
        {
            m_context.addServiceListener(sl,
                "(objectClass="
                + org.apache.felix.shell.ShellService.class.getName()
                + ")");
        }
        catch (InvalidSyntaxException ex)
        {
            System.err.println("ShellTui: Cannot add service listener.");
            System.err.println("ShellTui: " + ex);
        }

        // Now try to manually initialize the impl service
        // since one might already be available.
        initializeService();
    }

    private synchronized void initializeService()
    {
        if (m_shell == null)
        {
            m_shellRef = m_context.getServiceReference(
                org.apache.felix.shell.ShellService.class.getName());
            if (m_shellRef != null)
            {
                m_shell = (ShellService) m_context.getService(m_shellRef);
                //register the Android UI osgi service
                m_context.registerService(ViewFactory.class.getName(), m_Factory, null);
            }
        }
    }

    public void stop(BundleContext context)
    {

    }

    //This is where we build the Android UI osgi service
    private class AndroidViewprovider implements ViewFactory{

		private Activity activ;
		private LinearLayout m_main;
		private EditText m_text;
		private Button m_button;
		private ScrollView m_scroll;
		private LinearLayout m_sub;
		private TextView m_out;
		private PrintStream out;

		public View create(Activity activity) {
	        activ = activity;
	      
	        out = new PrintStream(new OutputStream(){
	        	ByteArrayOutputStream output = new ByteArrayOutputStream();

				public void write(int oneByte) throws IOException {
					output.write(oneByte);
					if (oneByte == '\n') {
						Log.v("out", new String(output.toByteArray()));
						activ.runOnUiThread(new Runnable() {
							public void run() {
								m_out.setText(m_out.getText()+output.toString()+"\n");
							}
						});
						output = new ByteArrayOutputStream();
					}
				}});
	        
	        m_main = new LinearLayout(activ);
	        m_main.setOrientation(LinearLayout.VERTICAL);
	        
	        m_scroll = new ScrollView(activ);
	        
	        m_out = new TextView(activ);
	        
	        m_sub = new LinearLayout(activ);
	        m_sub.setOrientation(LinearLayout.HORIZONTAL);
	        
	        m_text = new EditText(activ);
	        m_text.setLines(1);
	        
	        m_button = new Button(activ);
	        m_button.setText("Execute");
			m_button.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					if (m_shell == null) {
						System.out.println("No shell service available.");
					} else {
						try {
							m_out.setText("");
							m_shell.executeCommand(m_text.getText().toString(),
									out, out);
							m_text.setText("");
						} catch (Exception ex) {
							System.err.println("ShellTUI: " + ex);
							ex.printStackTrace();
						}
					}
				}

			});
	       
			m_scroll.addView(m_out,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,1));
	        m_main.addView(m_scroll,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT,1));
	        m_sub.addView(m_text,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT,1));
	        m_sub.addView(m_button,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,1));
	        m_main.addView(m_sub,new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT,1));
	        
	        m_main.setVisibility(View.VISIBLE);
	        return m_main;
		}
    	
    }

}