/*	
	Copyright 2008-2010 Fraunhofer IGD, http://www.igd.fraunhofer.de
	Fraunhofer-Gesellschaft - Institute of Computer Graphics Research 
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.persona.middleware.context;

import org.osgi.framework.BundleContext;
import org.persona.middleware.Activator;
import org.persona.ontology.context.ContextProvider;

import de.fhg.igd.ima.sodapop.Bus;
import de.fhg.igd.ima.sodapop.Publisher;
import de.fhg.igd.ima.sodapop.msg.Message;

/**
 * Provides the interface to be implemented by context publishers
 * together with shared code. Only instances of this class can 
 * publish context events.
 * The convention of the context bus regarding the registration parameters is the
 * following:
 * <ul><li>ContextPublishers provide only at the registration time info about themselves using
 * {@link org.persona.ontology.context.ContextProvider}.</li>
 * <li>ContextSubscribers may pass an array of {@link org.persona.middleware.context.ContextEventPattern}s
 * as their initial subscriptions and can always add new (and remove old) subscriptions dynamically.</li>
 * </ul>
 * 
 * @author mtazari
 */
public abstract class ContextPublisher implements Publisher {
	private ContextBus bus;
	private String myID;
	private ContextProvider providerInfo;
	
	protected ContextPublisher(BundleContext context, ContextProvider providerInfo) {
		Activator.checkContextBus();
		bus = (ContextBus) context.getService(
				context.getServiceReference(ContextBus.class.getName()));
		myID = bus.register(this);
		this.providerInfo = providerInfo;
	}

	public abstract void communicationChannelBroken();

	public final boolean eval(Message m) {
		return false;
	}

	public final void handleRequest(Message m) {
	}

	public final void busDyingOut(Bus b) {
		if (b == bus)
			communicationChannelBroken();
	}
	
	public final void publish(ContextEvent e) {
		if (e != null) {
			if (e.getProvider() == null  &&  providerInfo != null)
				e.setProvider(providerInfo);
			else if (providerInfo != e.getProvider())
				return;
			bus.sendMessage(myID, e);
		}
	}
	
	public void close(){
		bus.unregister(myID, this);
	}
}
